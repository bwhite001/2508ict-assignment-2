/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.CustomComponents;
import CSP_Source.NurseStartingInfomation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author s2843411
 */
public class MultiToggle extends JButton
{
    
    private String[] toggleText;
    private int value;
    
    public MultiToggle(String[] str) {
        toggleText = str;
        value = 0;
        addActionListener(new ButtonListener());
        super.setText(toggleText[value]);
    }
    public void buttonClick()
    {
        if(value < toggleText.length)
        {
            value++;
        }
        else
        {
            value = 0;
        }
        
        super.setText(toggleText[value]);
    }
    public int getValue()
    {
        return value;
    }
    
    private class ButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
            buttonClick();
        }
    }
}