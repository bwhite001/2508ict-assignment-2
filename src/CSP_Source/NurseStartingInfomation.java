package CSP_Source;

public class NurseStartingInfomation {
    private int shiftType; // 0 = D, 1 = N, 2 = DN
    private int grade; // 0 = RN, 1 = SRN
    private int numShift;
    private int lastShift; // 0 = Off, 1 = Day, 2 = Night
    private int lastOff;

    private int[] presetWeek;

    public NurseStartingInfomation(int shiftType, int grade, int numShift, int lastOff,int currentWorkingDays, int[] presetWeek)
    {
       this.lastOff = currentWorkingDays;
       this.shiftType = shiftType;
       this.grade = grade;
       this.numShift = numShift;
       this.lastShift = lastOff;
       this.presetWeek = presetWeek;
    }
    public int[] getPresetWeek()
    {
        return presetWeek;
    }
    public int getShiftType()
    {
        return shiftType;
    }
    public boolean isSRN()
    {
        return (grade == 1);
    }
    private String getShiftTypeChr()
    {
        if(shiftType == 1)
            return " N";
        else if(shiftType == 2)
            return "DN";
        else
            return " D";
    }
    
    public char getLastShiftTypeChr()
    {
        if(lastShift == 1)
            return 'D';
        else if(lastShift == 2)
            return 'N';
        else
            return 'O';
    }
    private String getGradeChr()
    {
        if(grade == 1)
            return "SRN";
        else
            return " RN";
    }
    public int getNumShift()
    {
        return numShift;
    }
    public int getLastShift()
    {
        return lastShift;
    }
    public int getLastOff()
    {
        return lastOff;
    }
    public String toString()
    {
        return "Type: "+getShiftTypeChr()+" | Grade: "+getGradeChr()+" | Shifts: "+numShift+" | Last Off: "+getLastShiftTypeChr()+" | Time Off: "+lastOff + " | Roster: " + ConsoleCommands.intArrayToString(presetWeek);
    }
}
