package CSP_Source;

public class ConsoleCommands {

    public static String getWhiteSpace(int length)
    {
        String ws = "";
        for(int i = 0; i<length; i++)
        {
            ws += " ";
        }
        return ws;
    }
    public static String lineOfLines(int length)
    {
        String lines = "";
        for(int i = 0; i<length; i++)
        {
            lines += "-";
        }

        return lines;
    }


    public static String intArrayToString(int[] array)
    {
        String out = "|";

        for(int i = 0; i<array.length; i++)
        {
            out += " "+array[i]+" |";
        }

        return out;
    }

}
