/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import App.NurseRosterController;
import CSP_Source.NurseStartingInfomation;
import javax.swing.table.*;
import javax.swing.*;
import javax.swing.JOptionPane.*;

public class MainWindow extends javax.swing.JFrame {

    private NurseRosterController controller;
    
    private String[] nurseTableHeaders = {"Nurse", "Shift Pattern", "Grade", "Num Shifts", "Last Off", "Last Shift"};
    private String[] weekTitles = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    private String[] weekBigTitles = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
    private String[] minShiftHeader = {"MinShifts (Day)","Total Rostered Day Nurses","MinShifts (Night)", "Total Rostered Night Nurses"};
    
    private String[] rosterValues = {"Auto","D","N", "O"};
    private String[] lastShiftValues = {"O","D", "N"};
    private String[] shiftTypes = {"D","N","DN"};
    private String[] nurseRank = {"RN", "SRN"};
    
    
    
    private JComboBox rosterCombos = new JComboBox(rosterValues);
    private JComboBox lastShiftCombos = new JComboBox(lastShiftValues);
    private JComboBox shiftTypeButton = new JComboBox(shiftTypes);
    private JComboBox nurseRankButton = new JComboBox(nurseRank);
    
    
    
    public MainWindow(NurseRosterController controller) { 
        this.controller = controller;
        
        initComponents();
        setUpTables();
        open();
        
        super.pack();
    }
    
    public void setUpTables()
    {
      createRosterTable(false, nurseTable);
    }
    public void open() {
        genrateBtn.setEnabled(false);
        setVisible(true);
    }
    
    public void createRosterTable(boolean fortnight, JTable table)
    {
        String[] tableTitles = getTableTitle(fortnight);
        
        setupTable(new Object[0][tableTitles.length], tableTitles);
        
    }
    
    public void setupTable(Object[][] dataVector, Object[] columnIdentifiers)
    {
        DefaultTableModel model = new DefaultTableModel();
        nurseTable.setModel(model);
        nurseTable.setColumnSelectionAllowed(true);
        
        model.setDataVector(dataVector, columnIdentifiers);
        
        for(int i = 0; i<columnIdentifiers.length; i++)
        {
            if(i>=nurseTableHeaders.length)
            {
                nurseTable.getColumnModel().getColumn(i).setCellEditor(new DefaultCellEditor(rosterCombos));
            }
            nurseTable.getTableHeader().resizeAndRepaint();
        }
        nurseTable.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(shiftTypeButton));
        nurseTable.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(nurseRankButton));
        nurseTable.getColumnModel().getColumn(5).setCellEditor(new DefaultCellEditor(lastShiftCombos));
        
        
    }
    public String[] getTableTitle(boolean fortnight) {
        
        int weeklength = (fortnight)? 14 : 7;
        
        String[] out = new String[nurseTableHeaders.length+weeklength];
        
        for(int i = 0; i<out.length; i++)
        {
            String title;
            if(i<nurseTableHeaders.length)
            {
               title = nurseTableHeaders[i]; 
            }
            else
            {
              int j = (i - nurseTableHeaders.length) % weekTitles.length;
              int k = (i - nurseTableHeaders.length) / weekTitles.length + 1;
              title = weekTitles[j].toUpperCase() +"_"+ k;
              
            }
            
            out[i] = title;
        }
        
        return out;
    }
    public void generateNurseStart(boolean fortnight, int numNurses)
    {
        String[] tableTitles = getTableTitle(fortnight); 
        String[][] data = new String[numNurses][tableTitles.length];
        int[] joptions = new int[numNurses];
        int numD = numNurses/2;
        int numN = numNurses/5;
        for(int i = 0; i<data.length; i++)
        {
            String[] row = new String[tableTitles.length];
            row[0] = "Nurse "+(i+1);
            if(i<numD)
                row[1] = shiftTypes[0];
            else if(i<numD+numN)
                row[1] = shiftTypes[1];
            else
                row[1] = shiftTypes[2];
            
            row[2] = (i==numD || i == 0 || i == numD+numN || i == numD+numN-1 || i == numD+2*numN)? "SRN" : "RN";
            row[3] = (fortnight) ? "10" : "5";
            row[4] = "0";
            row[5] = "O";
            
            for(int j = 6; j<row.length; j++)
            {
                row[j] = "Auto";
            }

            data[i] = row;
        }
        
        setupTable(data, tableTitles);
        
    }
    
    public void generateNumShiftsTable(boolean fornight, int numNurses)
    {
        int weeklength = (fornight) ? 14 : 7;
        String[] tableTitles = new String[weeklength+1]; 
        
        
        String[][] data = new String[minShiftHeader.length][tableTitles.length];
        
        for(int i = 0; i<tableTitles.length; i++)
        {
            String title;
            if(i>0)
            {
              int j = (i - 1) % weekTitles.length;
              int k = (i - 1) / weekTitles.length + 1;
              title = weekBigTitles[j]+k;
            }
            else
            {
                title = "";
            }

            tableTitles[i] = title;
        }
        
        for(int i = 0; i < data.length; i++)
        {
            data[i][0] = minShiftHeader[i];
            for(int j = 1; j<data[i].length; j++)
            {
                if(i % 2 == 0)
                    if(tableTitles[j].substring(0, tableTitles[j].length()-1).equals("Saturday") || tableTitles[j].substring(0, tableTitles[j].length()-1).equals("Sunday"))
                    {
                        
                        int num = (i==0)? numNurses/2-1 : numNurses/6;
                        
                        if(num<0)
                        {
                            num = 0;
                        }
                        
                        data[i][j] = ""+num;
                    }
                    else
                        data[i][j] = "" + ((i==0)? numNurses/2 : numNurses/5);
                else
                  data[i][j] = "";  
            }
        }
        
        numShiftsTable.setModel(new DefaultTableModel(data, tableTitles));
    }
    public boolean sanitaryCheck()
    {
        int smallestN = getSmallestMin(1);
        int smallestD = getSmallestMin(0);
        
        int[] counts = getNurseCounts();

        String errStr = "";
        if(counts[0]+counts[2] < smallestN || smallestN == 0 && counts[0]+counts[2] <= 0)
        {
            errStr += "There is not Enough Day Nurses! \n";
        }
        if(counts[1]+counts[2] < smallestN || smallestN == 0 && counts[1]+counts[2] <= 0)
        {
           errStr += "There is not Enough Night Nurses! \n"; 
        }
        
        
        if(errStr.length()>0)
            JOptionPane.showMessageDialog(null,errStr);
        return errStr.length() == 0;
    }
    public int[] getNurseCounts()
    {
        int[] count = new int[3];
        int rows = nurseTable.getModel().getRowCount();
        for(int i = 0; i < rows; i++)
        {

            String value = (String)nurseTable.getModel().getValueAt(i, 1);
            
            if(value.equals("D"))
            {
                count[0]++;
            }
            else if(value.equals("N"))
            {
                count[1]++;
            }
            else if(value.equals("DN"))
            {
                count[2]++;
            }
        }
        return count;
    }
    public int getSmallestMin(int night)
    {
       night = night*2;
       int smallest = Integer.parseInt((String)numShiftsTable.getModel().getValueAt(night, 1));
       
       int length = numShiftsTable.getModel().getColumnCount();
       
       for(int i = 2; i<length; i++)
       {
           Object obj = numShiftsTable.getModel().getValueAt(night, i);
           if((Integer.parseInt((String) obj))< smallest)
               smallest = Integer.parseInt((String) obj);
           
       }
       
       return smallest;
    }
    public NurseStartingInfomation[] getNursesInfo()
    {
        
        
        int rows = nurseTable.getModel().getRowCount();
        NurseStartingInfomation[] out = new NurseStartingInfomation[rows];
        
        
        for(int i = 0; i < rows; i++)
        {
            int shiftType = getValue(nurseTable.getModel().getValueAt(i, 1).toString(), shiftTypes);
            int grade = getValue(nurseTable.getModel().getValueAt(i, 2).toString(), nurseRank);
            
            int numShift = Integer.parseInt(nurseTable.getModel().getValueAt(i, 3).toString());
            
            int lastOff = Integer.parseInt(nurseTable.getModel().getValueAt(i, 4).toString());
            
            int lastShift = getValue(nurseTable.getModel().getValueAt(i, 5).toString(), lastShiftValues);
            
            
            
            int[] week = getIntWeek(i);
            
            out[i] = new NurseStartingInfomation(shiftType,grade,numShift,lastShift,lastOff,week);
        }
        
        return out;
    }
    
    public int[] getIntWeek(int row) {
        int[] output = new int[nurseTable.getModel().getColumnCount()-nurseTableHeaders.length];
        
        int col = nurseTable.getModel().getColumnCount();
        
        for(int i = nurseTableHeaders.length; i<col; i++)
        {
            String value = (String)nurseTable.getModel().getValueAt(row, i);
            int tmp;
            
            switch (value) {
                case "D":
                    tmp = 1;
                    break;
                case "N":
                    tmp = 2;
                    break;
                case "O":
                    tmp = 0;
                    break;
                default:
                    tmp = -1;
            }
            output[i-nurseTableHeaders.length] = tmp;
        }
        return output;
    }
    
    
    public int getValue(String value, String[] checkValues)
    {
        for(int i = 0; i<checkValues.length; i++)
        {
            if(value.equals(checkValues[i]))
            {
                return i;
            }
        }

       return 0;
    }
    
    public int[] getConstraints(int night)
    {
       night = night*2;
       int length = numShiftsTable.getModel().getColumnCount()-1;
       
       int[] out = new int[length];
       
       for(int i = 0; i<out.length; i++)
       {
           String obj = numShiftsTable.getModel().getValueAt(night, i+1).toString();
           
           out[i] = Integer.parseInt(obj);
       }
       
       return out;
    }
    public void updateTableInfo(char[][] data, int[] dayCon, int[] nightCon)
    {
        setConstraintStats(0, dayCon);
        setConstraintStats(1, nightCon);
        
        setRosterData(data);
        
        genrateBtn.setEnabled(false);
        printConsole("Roster Done! \n");
    }
    
    public void setRosterData(char[][] data)
    {
        for(int i = 0; i < data.length; i++)
        {
            for(int j = 0; j< data[i].length; j++)
            {
                
                nurseTable.getModel().setValueAt(data[i][j], i, j+6); 
            }
        }
    }
    public void setConstraintStats(int night, int[] values)
    {
       night = night*2+1;
       int length = numShiftsTable.getModel().getColumnCount()-1;
       
       
       for(int i = 0; i<values.length; i++)
       {
           numShiftsTable.getModel().setValueAt(values[i], night, i+1);
       }
    }
    public void printConsole(String text)
    {
        console.setText(console.getText()+text);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rosterValuesButtonGroup = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        buttonGroup1 = new javax.swing.ButtonGroup();
        createRosterPanel = new javax.swing.JPanel();
        numNursesSpinner = new javax.swing.JSpinner();
        numNursesLBL = new javax.swing.JLabel();
        day7Option = new javax.swing.JRadioButton();
        day14Option = new javax.swing.JRadioButton();
        createBtn = new javax.swing.JButton();
        nurseRosterPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        nurseTable = new javax.swing.JTable();
        numOfShifts = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        numShiftsTable = new javax.swing.JTable();
        genrateBtn = new javax.swing.JButton();
        statusPannel = new javax.swing.JPanel();
        scroll = new javax.swing.JScrollPane();
        console = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        optButtons = new javax.swing.JRadioButton();
        notOptButton = new javax.swing.JRadioButton();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Nurse Roster Assignment - Brandon & Justin");
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        createRosterPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Create Roster", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("SansSerif", 0, 14))); // NOI18N

        numNursesSpinner.setModel(new javax.swing.SpinnerNumberModel(3, 3, 99, 1));
        numNursesSpinner.setValue(3);

        numNursesLBL.setText("Number of nurses");

        rosterValuesButtonGroup.add(day7Option);
        day7Option.setSelected(true);
        day7Option.setText("7 Day roster");
        day7Option.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                day7OptionActionPerformed(evt);
            }
        });

        rosterValuesButtonGroup.add(day14Option);
        day14Option.setText("14 Day roster");

        createBtn.setText("Create Roster");
        createBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout createRosterPanelLayout = new javax.swing.GroupLayout(createRosterPanel);
        createRosterPanel.setLayout(createRosterPanelLayout);
        createRosterPanelLayout.setHorizontalGroup(
            createRosterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createRosterPanelLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(numNursesLBL)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(numNursesSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(267, 267, 267)
                .addGroup(createRosterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(createRosterPanelLayout.createSequentialGroup()
                        .addComponent(day14Option)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 436, Short.MAX_VALUE)
                        .addComponent(createBtn))
                    .addGroup(createRosterPanelLayout.createSequentialGroup()
                        .addComponent(day7Option)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        createRosterPanelLayout.setVerticalGroup(
            createRosterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(createRosterPanelLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(createRosterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(numNursesSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(numNursesLBL)
                    .addComponent(day7Option))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(createRosterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(createRosterPanelLayout.createSequentialGroup()
                        .addGap(0, 20, Short.MAX_VALUE)
                        .addComponent(createBtn))
                    .addGroup(createRosterPanelLayout.createSequentialGroup()
                        .addComponent(day14Option)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        nurseRosterPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Roster", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("SansSerif", 0, 14))); // NOI18N

        nurseTable.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        nurseTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(nurseTable);

        javax.swing.GroupLayout nurseRosterPanelLayout = new javax.swing.GroupLayout(nurseRosterPanel);
        nurseRosterPanel.setLayout(nurseRosterPanelLayout);
        nurseRosterPanelLayout.setHorizontalGroup(
            nurseRosterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(nurseRosterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        nurseRosterPanelLayout.setVerticalGroup(
            nurseRosterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(nurseRosterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                .addContainerGap())
        );

        numOfShifts.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Num Of Shifts", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("SansSerif", 0, 14))); // NOI18N

        numShiftsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(numShiftsTable);

        javax.swing.GroupLayout numOfShiftsLayout = new javax.swing.GroupLayout(numOfShifts);
        numOfShifts.setLayout(numOfShiftsLayout);
        numOfShiftsLayout.setHorizontalGroup(
            numOfShiftsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(numOfShiftsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 1049, Short.MAX_VALUE)
                .addContainerGap())
        );
        numOfShiftsLayout.setVerticalGroup(
            numOfShiftsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, numOfShiftsLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        genrateBtn.setText("Generate Roster");
        genrateBtn.setToolTipText("");
        genrateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genrateBtnActionPerformed(evt);
            }
        });

        statusPannel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Console", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("SansSerif", 0, 14))); // NOI18N

        console.setColumns(20);
        console.setRows(5);
        scroll.setViewportView(console);

        jButton1.setText("Clear Console");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Clear All");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout statusPannelLayout = new javax.swing.GroupLayout(statusPannel);
        statusPannel.setLayout(statusPannelLayout);
        statusPannelLayout.setHorizontalGroup(
            statusPannelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPannelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(statusPannelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scroll)
                    .addGroup(statusPannelLayout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 869, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        statusPannelLayout.setVerticalGroup(
            statusPannelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, statusPannelLayout.createSequentialGroup()
                .addComponent(scroll, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(statusPannelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)))
        );

        jLabel1.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel1.setText("Run Optimized:");

        buttonGroup1.add(optButtons);
        optButtons.setText("Yes");

        buttonGroup1.add(notOptButton);
        notOptButton.setSelected(true);
        notOptButton.setText("No");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(createRosterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(nurseRosterPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(numOfShifts, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(statusPannel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(optButtons)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(notOptButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(genrateBtn)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(createRosterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(nurseRosterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(numOfShifts, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(genrateBtn)
                    .addComponent(jLabel1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(optButtons)
                        .addComponent(notOptButton)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusPannel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void day7OptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_day7OptionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_day7OptionActionPerformed

    private void createBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createBtnActionPerformed
        console.setText("");
        int numNurses = (int)numNursesSpinner.getValue();
        boolean fortnight = day14Option.isSelected();
        if(numNurses < 3)
        {
            JOptionPane.showMessageDialog(null, "Number Of Nurses Must be greater than 2!");
            genrateBtn.setEnabled(false);
        }
        else
        {
            generateNurseStart(fortnight, numNurses);
            generateNumShiftsTable(fortnight, numNurses);
        }
        genrateBtn.setEnabled(numNurses >= 2);
    }//GEN-LAST:event_createBtnActionPerformed

    private void genrateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_genrateBtnActionPerformed
        if(sanitaryCheck())
        {
            boolean opt = optButtons.isSelected();
            
            printConsole((opt)? "Finding Optimal Solution Please Wait.. \n" :  "Finding Satisfiable Solution Please Wait.. \n");
            controller.getGUIData(opt);
        }
    }//GEN-LAST:event_genrateBtnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        console.setText("");
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        controller.resetApp();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        numNursesSpinner.requestFocus();
    }//GEN-LAST:event_formMouseClicked

    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JTextArea console;
    private javax.swing.JButton createBtn;
    private javax.swing.JPanel createRosterPanel;
    private javax.swing.JRadioButton day14Option;
    private javax.swing.JRadioButton day7Option;
    private javax.swing.JButton genrateBtn;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JRadioButton notOptButton;
    private javax.swing.JLabel numNursesLBL;
    private javax.swing.JSpinner numNursesSpinner;
    private javax.swing.JPanel numOfShifts;
    private javax.swing.JTable numShiftsTable;
    private javax.swing.JPanel nurseRosterPanel;
    private javax.swing.JTable nurseTable;
    private javax.swing.JRadioButton optButtons;
    private javax.swing.ButtonGroup rosterValuesButtonGroup;
    private javax.swing.JScrollPane scroll;
    private javax.swing.JPanel statusPannel;
    // End of variables declaration//GEN-END:variables

}
