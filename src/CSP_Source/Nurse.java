package CSP_Source;

public class Nurse {
    private static int nurseNum = 1;
    private NurseStartingInfomation info;
    private int currentNurse = 0;

    private RosterWeek currentRoster;
    public int getCurrentNurse()
    {
        return currentNurse;
    }
    public Nurse(Nurse oldNurse)
    {
        this.currentNurse = oldNurse.getCurrentNurse();
        this.info = oldNurse.getInfo();

        currentRoster = new RosterWeek(oldNurse.getCurrentRoster());
    }

    public Nurse(NurseStartingInfomation info, int weeklength) {
        currentNurse = nurseNum;
        nurseNum++;
        this.info = info;

        currentRoster = new RosterWeek(info.getPresetWeek(),weeklength, info.getShiftType());
        currentRoster.randomizeWeek(info.getNumShift());

    }

    public NurseStartingInfomation getInfo() {
       return this.info;
    }

    public String toString()
    {
        String tmp = "Nurse "+toIntStr(currentNurse)+" | "+info + " " + currentRoster;

        return tmp;
    }

    private String toIntStr(int i)
    {
        if(i < 10)
        {
            return "0"+i;
        }
        else
            return ""+i;
    }
    public RosterWeek getCurrentRoster()
    {
        return currentRoster;
    }
    public char getDay(int day)
    {
        
        int currentIntDay = currentRoster.getDay(day);
        if(currentIntDay > -1)
        {
            switch (currentIntDay)
            {
                case 1:
                    return 'D';
                case 2:
                    return 'N';
                default:
                    return 'O';
            }
        }
        else
        {
            return 'E';
        }
        
    }

    public void setDay(int day, int value)
    {
        int currentIntDay = currentRoster.getDay(day);
        if(rightValue(value) && !currentRoster.isFixed(day))
        {
            currentRoster.setDay(day, value);
        }
    }
    private boolean rightValue(int value)
    {
        int type = info.getShiftType();
        boolean check = (value == 1 || value == 0 || value == 2);
        if(check)
        {
            switch (type)
            {
                // Day Nurse
                case 0:
                    return (value != 2);
                // Night Nurse
                case 1:
                    return (value != 1);
                default:
                    return true;
            }
        }
        else
        {
            return false;
        }
    }
    public void randomWeek()
    {
        currentRoster.randomizeWeek(info.getNumShift());
    }
    private int getWorkingDays()
    {
        return currentRoster.getWorkingDays();
    }

    public int isFullyWorking()
    {
        int currentWorkingDays = getWorkingDays();
        int totalWorkingDays = getInfo().getNumShift();

        if(currentWorkingDays < totalWorkingDays)
        {
            return -1;
        }
        else if(currentWorkingDays > totalWorkingDays)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}

