package CSP_Source;

import GUI.MainWindow;

public class NurseRoster {

    private NurseStartingInfomation[] nurseInfo;

    private char[] weekdays = {'M','T','W','T','F','S','S'};
    private Nurse[] nurses;
    private int[][] colConstraints;
    private int weeklength;
    private Constraints constaintCalulator;

    private int lastConstraintCost = 0;
    private int currentConstraintCost = 0;

    private int[] counter = {0,0,0};

    public NurseRoster(int weeklength, NurseStartingInfomation[] nurseInfo, int[] dailyCon, int[] nightlyCon)
    {
        colConstraints = new int[2][weeklength];
        colConstraints[0] = dailyCon;
        colConstraints[1] = nightlyCon;

        this.nurseInfo = nurseInfo;

        this.weeklength = weeklength;

        constaintCalulator = new Constraints(colConstraints, weeklength);
    }

    public void generate(boolean opt, MainWindow mw)
    {
        int weight = (opt) ? 0 : 200;
        int sameCount = 0;
        int currentNurse = 0;
        randomizeData();

        lastConstraintCost = constaintCalulator.checkConstraints(nurses);
        currentConstraintCost = lastConstraintCost;
        // while loop checking constraintPoints with a minimum weight

        currentConstraintCost = constaintCalulator.checkConstraints(nurses);
        while (currentConstraintCost >= weight)
        {
            
            if(currentNurse < nurses.length)
                nurses = getBetterResults(currentNurse);
            else
            {
                currentNurse = 0;
                nurses = getBetterResults(0);
            }
            
            currentConstraintCost = constaintCalulator.checkConstraints(nurses);

            if (lastConstraintCost == currentConstraintCost)
            {
                sameCount++;
            }
            else
            {
                sameCount = 0;
            }

            if (sameCount == 300)
            {
                currentNurse = 0;
                
                nurses = semiRestart();
                constaintCalulator.checkConstraints(nurses);
                
                counter[1]++;
                sameCount = 0;
                
                if(!opt)
                  weight+= 5*nurses.length;
            }
            currentNurse++;
            
            counter[0]++;
            lastConstraintCost = currentConstraintCost;
            System.out.println("Constraint Value: " +lastConstraintCost+ "\n Weight:" +weight+" \n "+ConsoleCommands.intArrayToString(constaintCalulator.getConstraintValues()));
        }
    }

    public Nurse[] getBetterResults(int nurseIndex)
    {
        Nurse[] instancesOfNurses = new Nurse[weeklength];
        int[] instancesOfTotalConstants = new int[weeklength];

        int nIndex = nurseIndex;//(int)(Math.random()*nurses.length);
        Nurse someNurse = new Nurse(nurses[nIndex]);

        for(int i = 0; i<instancesOfNurses.length; i++)
        {
            int jValue = 0;
            int jCon = 0;
            for(int j = 0; j < 3; j++)
            {
               if(j == 0)
               {
                   jValue = 0;
                   jCon = changeNurse(nIndex, i, j);
               }
               else
               {
                   int tmp = changeNurse(nIndex, i, j);
                   if(tmp < jCon)
                   {
                       jCon = tmp;
                       jValue = j;
                   }
               }

            }

            Nurse tmp = new Nurse(someNurse);

            Nurse[] nursesTemp = copyNurses();

            tmp.setDay(i,jValue);

            nursesTemp[nIndex] = tmp;

            instancesOfTotalConstants[i] = constaintCalulator.checkConstraints(nursesTemp);

            instancesOfNurses[i] = tmp;

        }

        int index = 0;
        int smallestValue = instancesOfTotalConstants[0];
        for(int i = 0; i<instancesOfTotalConstants.length; i++)
        {
            if(instancesOfTotalConstants[i] > smallestValue)
            {
                index = i;
                smallestValue = instancesOfTotalConstants[i];
            }

        }
        Nurse[] newNurses = copyNurses();
        if(instancesOfTotalConstants[index] >= currentConstraintCost)
        {
            newNurses[nIndex] = instancesOfNurses[index];
        }

        return newNurses;
    }
    public int changeNurse(int index, int day, int value)
    {
        int out = 0;
        Nurse tmp = new Nurse(nurses[index]);

        tmp.setDay(day, value);

        Nurse[] nursesTemp = copyNurses();

        nursesTemp[index] = tmp;

        out = constaintCalulator.checkConstraints(nursesTemp);
        counter[2]++;
        return out;
    }
    public Nurse[] copyNurses()
    {
        Nurse[] outNurses = new Nurse[nurses.length];

        for(int i = 0; i<nurses.length; i++)
        {
           outNurses[i] = new Nurse(nurses[i]);
        }

        return outNurses;
    }
    public Nurse[] semiRestart()
    {
        Nurse[] thisNurses = copyNurses();


        int randomizeTimes = (int)(Math.random()*weeklength*2);

        for(int i = 0; i<randomizeTimes; i++)
        {
           int randomNurseIndex = (int)(Math.random()*nurses.length);
           thisNurses[randomNurseIndex].randomWeek();
        }

        return thisNurses;
    }

    public void randomizeData()
    {
        nurses = new Nurse[nurseInfo.length];
        for(int i = 0; i< nurses.length; i++)
        {
            nurses[i] = new Nurse(nurseInfo[i], weeklength);
        }

        nurses = getBetterResults(0);
    }

    public String getTitle()
    {
        String out = "";
        int j = 0;
        for(int i = 0; i<weeklength; i++)
        {
          j = i % weekdays.length;
          out += " " + weekdays[j]+" ";
          out += "|";
        }

        return out;
    }
    public int getNumNurses(int day, char dayOrNight)
    {
        int count = 0;

        for(int i = 0; i< nurses.length; i++)
        {
            if(nurses[i].getDay(day) ==  dayOrNight)
            {
                count++;
            }
        }

        return count;
    }
    public char[][] getOutput()
    {
        char[][] out = new char[nurses.length][weeklength];
        
        for(int i = 0; i<out.length; i++)
        {
            for(int j = 0; j<out[i].length; j++)
            {
                out[i][j] = nurses[i].getDay(j);
            }
        }
        return out;        
    }
    public int[] getTotalNumNurses(char dayOrNight)
    {
        int length = (dayOrNight == 'D') ? colConstraints[0].length : colConstraints[1].length;

        int[] out = new int[length];

        for(int i = 0; i<out.length; i++)
        {
            out[i] = getNumNurses(i, dayOrNight);
        }

        return out;
    }
    public String colStats()
    {
        String output = "";

        output += ConsoleCommands.getWhiteSpace(22)+
                "Total Day Shifts"+
                ConsoleCommands.getWhiteSpace(21)+
                ConsoleCommands.intArrayToString(getTotalNumNurses('D'))+
                "\n";

        String line = ConsoleCommands.lineOfLines(output.length()-1);

        output += ConsoleCommands.getWhiteSpace(22)+
                "MinShifts(Day)"+
                ConsoleCommands.getWhiteSpace(23)+
                ConsoleCommands.intArrayToString(colConstraints[0])+
                "\n";



        output += line+"\n";

        output += ConsoleCommands.getWhiteSpace(22)+
                "Total Night Shifts"+
                ConsoleCommands.getWhiteSpace(19)+
                ConsoleCommands.intArrayToString(getTotalNumNurses('N'))+
                "\n";
        output += ConsoleCommands.getWhiteSpace(22)+
                "MinShifts(Night)"+
                ConsoleCommands.getWhiteSpace(21)+
                ConsoleCommands.intArrayToString(colConstraints[1])+
                "\n";
        output += line+"\n";
        return output;
    }
    public String toString()
    {
        String output;
        output = "Total Loops: "+counter[0]+" Nodes Expanded: "+ counter[2]+ " Semi Restarts "+ counter[1]+"\n";
        constaintCalulator.checkConstraints(nurses);
        output += constaintCalulator;

        return output;
    }
}