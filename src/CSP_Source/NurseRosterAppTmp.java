package CSP_Source;

import CSP_Source.NurseRoster;
import CSP_Source.NurseStartingInfomation;

public class NurseRosterAppTmp {


    public static void main(String[] args)
    {
        int numSRN = 3;
        int numNurses = 10;
        int weekLength = 7;

        NurseStartingInfomation[] nursesInfo = getNurseInfo(numNurses, weekLength);

        int[] dayNurses = getDailyAmmoutArray(2, numNurses, weekLength);
        int[] nightlyNurses = getDailyAmmoutArray(1, numNurses, weekLength);


        NurseRoster currentNurseRoster = new NurseRoster(weekLength, nursesInfo, dayNurses, nightlyNurses);

        System.out.print(currentNurseRoster);
    }


    public static NurseStartingInfomation[] getNurseInfo(int amount, int weeklength)
    {

        int numShift = (weeklength == 7) ? 5 : 10;
        int[] presetWeek = new int[weeklength];


        for(int i = 0; i < weeklength; i++)
        {
           presetWeek[i] = -1;
        }

        NurseStartingInfomation[] nursesInfo = new NurseStartingInfomation[amount];

        for(int i = 0; i < nursesInfo.length; i++)
        {
            int shiftType = (int)(Math.random()*3); // 0 = D, 1 = N, 2 = DN
            int grade = (i>3) ? 0 : 1; // 0 = RN, 1 = SRN
            int lastOff = 0; // 0 = Off, 1 = Day, 2 = Night
            int currentlyWorked = 0; // Num of day worked

            nursesInfo[i] = new NurseStartingInfomation(shiftType,grade,numShift,lastOff,currentlyWorked, presetWeek);
        }

        return nursesInfo;
    }

    public static int[] getDailyAmmoutArray(int type, int amount, int weeklength)
    {
        int[] output = new int[weeklength];

        for(int i = 0; i < output.length; i++)
        {
            output[i] = amount/4*(type);
        }

        return output;
    }
}
