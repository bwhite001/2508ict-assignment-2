package CSP_Source;

public class RosterWeek {
    private int type;  // 0 = D, 1 = N, 2 = DN
    private int[] initRoster;
    private int[] currentWeek;


    public RosterWeek(RosterWeek oldRoster)
    {
        this.currentWeek = oldRoster.getCurrentWeek();
        this.initRoster = oldRoster.getInitRoster();

        this.type = oldRoster.getType();
    }
    public RosterWeek(int[] presetWeek, int weeklength, int type)
    {
        currentWeek = new int[weeklength];

        for(int i = 0; i<weeklength; i++)
        {
            if(presetWeek[i] > -1)
                currentWeek[i] = presetWeek[i];
        }

        this.initRoster = presetWeek;
        this.type = type;
    }
    public int[] getInitRoster()
    {
        return initRoster;
    }
    public int[] getCurrentWeek()
    {
        return currentWeek;
    }
    public int getType()
    {
        return type;
    }
    public void randomizeWeek(int daysOn)
    {
        int count = 0;

        for(int i = 0; count<daysOn && i<currentWeek.length; i++)
        {
                int value = randomizeDay(daysOn, i);
                if(value > 0)
                {
                    currentWeek[i] = value;
                    count ++;
                }
        }
    }
    public int randomizeDay(int daysOn, int i) // i = the day;
    {
      int value = 0;
      if(getWorkingDays() < daysOn)
      {
          if(initRoster[i] == -1)
          {

              if(type == 2)
              {
                  value = (int)(Math.random()*2);
              }
              else
              {
                  value = (int)(Math.random()*2);

                  if(type == 1)
                  {
                      if(value == 1)
                          value = 2;
                  }
                  else
                  {
                      if(value == 2)
                          value = 1;
                  }
              }
          }
          else
          {
              value = initRoster[i];
          }
      }
      else
      {
          value = currentWeek[i];
      }

      return value;
    }

    public int getWorkingDays()
    {
        int count = 0;
        for(int i = 0; i<currentWeek.length; i++)
        {
            if(currentWeek[i] > 0)
                count++;
        }

        return count;
    }
    public int getDay(int day)
    {
        if(day >= 0 && day < currentWeek.length)
        {
            return currentWeek[day];
        }
        else
        {
            return -1;
        }
    }
    public void setDay(int day, int value)
    {
        if(day >= 0 && day < currentWeek.length)
        {
           currentWeek[day] = value;
        }
    }

    public boolean isFixed(int day)
    {
        if(day >= 0 && day < currentWeek.length)
        {
            return (initRoster[day] != -1);
        }
        else
            return false;
    }
    public String toString()
    {
        String out = "|";

        for(int i = 0; i<currentWeek.length; i++)
        {
            switch (currentWeek[i])
            {
                case 1:
                    out += " D ";
                    break;
                case 2:
                    out += " N ";
                    break;
                default:
                    out += " O ";
            }

            out += "|";
        }

        return out;
    }
}
