package CSP_Source;

public class Constraints {
    private static final String[] CONSTRAINT_TITLES = {"SRN","Num Shifts","Max Days","Night Shift","Minimum Day Shift","Minimum Night Shift","Max Rostered Days","Nurse Type"};
    private static final short MAX_DAY_WEEK		= 5;
    private static final short MAX_DAY_FORTNIGHT	= 10;
    private static final short MAX_WORKING_DAYS	= 7;

    private static int weeklength = 0;
    private static int[] minimumDays;
    private static int[] minimumNights;

    private int constraintPoints;
    private Nurse[] currentNurses;

    public Constraints(int[][] colCon, int week_length)
    {
        weeklength = week_length;

        minimumDays = colCon[0];
        minimumNights = colCon[1];

    }

    public int checkConstraints(Nurse[] nurses)
    {
        int constraintPoints = 0;


        currentNurses = nurses;
        int[] constraints = getConstraintValues();

        for(int constrantValue : constraints)
        {
            constraintPoints += constrantValue;
        }

        return constraintPoints;
    }
    public int[] getConstraintValues() 
    {
       int[] out = new int[8];

       out[0] = srnConstraint(600);
       out[1] = numShiftsConstraint(100);
       out[2] = maxNumberOfWorkingDays(200);
       out[3] = nightShiftConstraint(600);
       out[4] = minimumDayShiftConstraint(200);
       out[5] = minimumNightShiftConstraint(200);
       out[6] = maxNumberOfRosteredDays(600);
       out[7] = shiftTypeConstraint(500);

       return out;
    }
    //Minimum SRN on;
    public int srnConstraint(int weight)
    {
        int srnDayCount = 0;
        int srnNightCount = 0;
        int finalCount = 0;

        for (int d = 0; d < weeklength; d++)
        {
            for (Nurse currentNurse : currentNurses) {
                if (currentNurse.getInfo().isSRN() && currentNurse.getDay(d) == 'D')
                    srnDayCount++;

                if (currentNurse.getInfo().isSRN() && currentNurse.getDay(d) == 'D')
                    srnNightCount++;
            }

            if (srnDayCount == 0)
            {
                finalCount++;
            }

            if (srnNightCount == 0)
            {
                finalCount++;
            }

            srnDayCount = 0;
            srnNightCount = 0;
        }
        return finalCount * weight;
    }

    // Checks the number of shifts a nurse if supposed to have
    public int numShiftsConstraint(int weight)
    {
        int dayOffCount = 0;
        int finalCount = 0;
        for (Nurse currentNurse : currentNurses) {
            for (int d = 0; d < weeklength; d++) {
                if (currentNurse.getDay(d) == 'O') {
                    dayOffCount++;
                }
            }
            int shiftCount = (weeklength - dayOffCount);
            
            int numShifts = currentNurse.getInfo().getNumShift();
            

            if (numShifts < shiftCount) {
                finalCount += 1;
            }

            dayOffCount = 0;
        }
        return finalCount * weight;
        
        
    }

    public int maxNumberOfWorkingDays(int weight)
    {
        int dayCount = 0;
        int finalCount = 0;
       
            for (Nurse currentNurse : currentNurses) {
               for (int d = 0; d < weeklength; d++)
                    {
                    dayCount = currentNurse.getInfo().getLastOff();
                    if (currentNurse.getDay(d) == 'D' || currentNurse.getDay(d) == 'N')
                        dayCount++;
                    if (currentNurse.getDay(d) == 'O')
                        dayCount = 0;

                    if (dayCount > MAX_WORKING_DAYS)
                    {
                        finalCount += Math.abs(dayCount - MAX_WORKING_DAYS);
                    }
                 }
                dayCount = 0;
                    }
        return finalCount * weight;
     }

    public int nightShiftConstraint(int weight)
    {
        int count = 0;

        for (Nurse currentNurse : currentNurses) {
            for (int d = 0; d < weeklength - 1; d++) {
                if(currentNurse.getInfo().getLastShiftTypeChr() == 'N' && currentNurse.getDay(0) == 'D')
                {
                    count++;
                }
                if (currentNurse.getDay(d) == 'N' && currentNurse.getDay(d + 1) == 'D') {
                    count++;
                }
            }
        }

        return count * weight;
    }
    // Minimum Shifts Day
    public int minimumDayShiftConstraint(int weight)
    {
        int dayCount = 0;
        int finalCount = 0;

        for (int d = 0; d < weeklength; d++)
        {
            for (Nurse currentNurse : currentNurses) {
                if (currentNurse.getDay(d) == 'D') {
                    dayCount++;
                }
            }

            if (dayCount < minimumDays[d])
            {
                finalCount += (minimumDays[d] - dayCount);
            }

            dayCount = 0;
        }
        return finalCount * weight;
    }

    // Minimum Shifts Night
    public int minimumNightShiftConstraint(int weight)
    {
        int nightCount = 0;
        int finalCount = 0;

        for (int d = 0; d < weeklength; d++)
        {
            for (Nurse currentNurse : currentNurses) {
                if (currentNurse.getDay(d) == 'N') {
                    nightCount++;
                }
            }

            if (nightCount < minimumNights[d])
            {
                finalCount += (minimumNights[d] - nightCount);
            }

            nightCount = 0;
        }
        return finalCount * weight;
    }


    // Max Number of Days per Roster
    public int maxNumberOfRosteredDays(int weight)
    {
        int dayCount = 0;
        int finalCount = 0;

        for (Nurse currentNurse : currentNurses) {
            dayCount = currentNurse.getInfo().getLastOff();
            for (int d = 0; d < weeklength; d++) {
                if (currentNurse.getDay(d) != 'O') {
                    dayCount++;
                }
            }

            if (weeklength == 14) {
                if (dayCount > MAX_DAY_FORTNIGHT) {
                    finalCount += (dayCount - MAX_DAY_FORTNIGHT);
                }
            } else {
                if (dayCount > MAX_DAY_WEEK) {
                    finalCount += (dayCount - MAX_DAY_WEEK);
                }
            }

            dayCount = 0;
        }
        return finalCount * weight;
    }

    // Checking if the shifts given fit with the nurse's type
    public int shiftTypeConstraint(int weight)
    {
        int problemCount = 0;
        int finalCount = 0;

        for (Nurse currentNurse : currentNurses) {
            for (int d = 0; d < weeklength; d++) {
                if (currentNurse.getInfo().getShiftType() == 0 && currentNurse.getDay(d) == 'N') {
                    problemCount++;
                } else if (currentNurse.getInfo().getShiftType() == 1 && currentNurse.getDay(d) == 'D') {
                    problemCount++;
                }

            }

            finalCount += problemCount;
            problemCount = 0;
        }
        return finalCount * weight;
    }

    public String toString()
    {
        String out = "";
        int[] currentCon = getConstraintValues();
        int total = 0;
        for(int i = 0; i < currentCon.length; i++)
        {
           total += currentCon[i];
           out += CONSTRAINT_TITLES[i]+" Constraint: "+currentCon[i]+"\n";
        }
        out += "Total Constraint: "+total;

        return out;
    }
}
