package App;
import CSP_Source.NurseRoster;
import CSP_Source.NurseStartingInfomation;
import GUI.*;
import java.io.Console;

public class NurseRosterController {
    private NurseRoster roster;
    private MainWindow mw;

    public NurseRosterController() {
       mw = new MainWindow(this);
    }
    public void resetApp()
    {
       mw.dispose();
       mw = new MainWindow(this);
    }
    public void getGUIData(boolean opt)
    {
        NurseStartingInfomation[] nurseInfo = mw.getNursesInfo();
        
        int[] dayConstraints = mw.getConstraints(0);
        int[] nightConstraints = mw.getConstraints(1);
        int weekLength = dayConstraints.length;
        
        roster = new NurseRoster(weekLength, nurseInfo, dayConstraints, nightConstraints);
        
        roster.generate(opt, mw);
        
        int[] dayStats = roster.getTotalNumNurses('D');
        int[] nightStats = roster.getTotalNumNurses('N');
        
        char[][] rosterData = roster.getOutput();
        
        mw.updateTableInfo(rosterData, dayStats, nightStats);
        
        mw.printConsole(roster.toString());
    }
}
